﻿// <copyright file="Volume.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Books.Models
{
    /// <summary>
    /// Volume model.
    /// </summary>
    public class Volume
    {
        /// <summary>
        /// Gets or sets the e-tag.
        /// </summary>
        /// <value>The e-tag.</value>
        public string ETag { get; set; }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the self link.
        /// </summary>
        /// <value>The self link.</value>
        public string SelfLink { get; set; }

        /// <summary>
        /// Gets or sets the volume information.
        /// </summary>
        /// <value>The volume information.</value>
        public VolumeInfo VolumeInfo { get; set; }
    }
}
