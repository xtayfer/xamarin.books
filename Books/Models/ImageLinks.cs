﻿// <copyright file="ImageLinks.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Books.Models
{
    /// <summary>
    /// Image links model.
    /// </summary>
    public class ImageLinks
    {
        /// <summary>
        /// Gets or sets the small thumbnail.
        /// </summary>
        /// <value>The small thumbnail.</value>
        public string SmallThumbnail { get; set; }

        /// <summary>
        /// Gets or sets the thumbnail.
        /// </summary>
        /// <value>The thumbnail.</value>
        public string Thumbnail { get; set; }
    }
}
