﻿// <copyright file="VolumeInfo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Books.Models
{
    using System;

    /// <summary>
    /// Volume info model.
    /// </summary>
    public class VolumeInfo
    {
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="VolumeInfo"/> allows anonymus logging.
        /// </summary>
        /// <value><c>true</c> if allows anonymus logging; otherwise, <c>false</c>.</value>
        public bool AllowAnonLogging { get; set; }

        /// <summary>
        /// Gets or sets the average rating.
        /// </summary>
        /// <value>The average rating.</value>
        public double AverageRating { get; set; }

        /// <summary>
        /// Gets or sets the page count.
        /// </summary>
        /// <value>The page count.</value>
        public int PageCount { get; set; }

        /// <summary>
        /// Gets or sets the ratings count.
        /// </summary>
        /// <value>The ratings count.</value>
        public int RatingsCount { get; set; }

        /// <summary>
        /// Gets or sets the canonical volume link.
        /// </summary>
        /// <value>The canonical volume link.</value>
        public string CanonicalVolumeLink { get; set; }

        /// <summary>
        /// Gets or sets the content version.
        /// </summary>
        /// <value>The content version.</value>
        public string ContentVersion { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the information link.
        /// </summary>
        /// <value>The information link.</value>
        public string InfoLink { get; set; }

        /// <summary>
        /// Gets or sets the language.
        /// </summary>
        /// <value>The language.</value>
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the maturity rating.
        /// </summary>
        /// <value>The maturity rating.</value>
        public string MaturityRating { get; set; }

        /// <summary>
        /// Gets or sets the print type.
        /// </summary>
        /// <value>The print type.</value>
        public string PrintType { get; set; }

        /// <summary>
        /// Gets or sets the published date.
        /// </summary>
        /// <value>The published date.</value>
        public string PublishedDate { get; set; }

        /// <summary>
        /// Gets or sets the publisher.
        /// </summary>
        /// <value>The publisher.</value>
        public string Publisher { get; set; }

        /// <summary>
        /// Gets or sets the preview link.
        /// </summary>
        /// <value>The preview link.</value>
        public string PreviewLink { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the authors.
        /// </summary>
        /// <value>The authors.</value>
        public string[] Authors { get; set; }

        /// <summary>
        /// Gets or sets the categories.
        /// </summary>
        /// <value>The categories.</value>
        public string[] Categories { get; set; }

        /// <summary>
        /// Gets or sets the image links.
        /// </summary>
        /// <value>The image links.</value>
        public ImageLinks ImageLinks { get; set; }

        /// <summary>
        /// Gets the author.
        /// </summary>
        /// <value>The author.</value>
        public string Author => this.Authors == null ? null : string.Join(", ", this.Authors);

        /// <summary>
        /// Gets the published date formatted.
        /// </summary>
        /// <value>The published date formatted.</value>
        public string PublishedDateFormatted
        {
            get
            {
                try
                {
                    var date = DateTime.Parse(this.PublishedDate);
                    return date.ToShortDateString();
                }
                catch (Exception)
                {
                    return this.PublishedDate;
                }
            }
        }
    }
}
