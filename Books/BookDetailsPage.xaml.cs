﻿// <copyright file="BookDetailsPage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Books
{
    using System;
    using Books.ModelControllers;
    using Xamarin.Forms;

    /// <summary>
    /// Book details page.
    /// </summary>
    public partial class BookDetailsPage : ContentPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BookDetailsPage"/> class.
        /// </summary>
        public BookDetailsPage()
        {
            this.InitializeComponent();
        }

        /// <inheritdoc/>
        protected override void OnAppearing()
        {
            base.OnAppearing();

            // Set actions.
            this.readButton.Clicked += this.ReadButton_Clicked;
        }

        /// <summary>
        /// Clicked on read button.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        private async void ReadButton_Clicked(object sender, EventArgs e)
        {
            await this.Navigation.PushAsync(new WebViewPage
            {
                Path = (this.BindingContext as VolumeController).VolumeInfo.PreviewLink,
            });
        }
    }
}
