﻿// <copyright file="WebViewPage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Books
{
    using System;
    using System.Collections.Generic;
    using Xamarin.Forms;

    /// <summary>
    /// Web view page.
    /// </summary>
    public partial class WebViewPage : ContentPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WebViewPage"/> class.
        /// </summary>
        public WebViewPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Gets or sets the path.
        /// </summary>
        /// <value>The path.</value>
        public string Path { get; set; }

        /// <inheritdoc/>
        protected override void OnAppearing()
        {
            base.OnAppearing();

            // Set data.
            this.webView.Source = this.Path;
        }
    }
}
