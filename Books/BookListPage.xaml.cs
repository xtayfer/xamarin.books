﻿// <copyright file="BookListPage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Books
{
    using System.Collections.Generic;
    using Books.ModelControllers;
    using Xamarin.Forms;

    /// <summary>
    /// Book list page.
    /// </summary>
    public partial class BookListPage : ContentPage
    {
        private uint currentPage;
        private List<VolumeController> volumes;

        /// <summary>
        /// Initializes a new instance of the <see cref="BookListPage"/> class.
        /// </summary>
        public BookListPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Gets or sets the query.
        /// </summary>
        /// <value>The query.</value>
        public string Query { get; set; }

        /// <inheritdoc/>
        protected override void OnAppearing()
        {
            base.OnAppearing();

            // Set data.
            this.currentPage = 0;
            this.Title = $"Search: {this.Query}";
            this.volumes = new List<VolumeController>();

            // Set actions.
            this.listView.ItemAppearing += this.ListView_ItemAppearing;
            this.listView.ItemSelected += this.ListView_ItemSelected;

            // Get remote data.
            this.GetRemoteData();
        }

        /// <summary>
        /// Gets the remote data.
        /// </summary>
        private void GetRemoteData()
        {
            this.activityIndicator.IsRunning = true;
            VolumeController.GetRemoteDataForQueryAtPage(this.Query, this.currentPage, (total, error, list) =>
            {
                this.activityIndicator.IsRunning = false;

                // Add new items.
                this.volumes.AddRange(list);
                System.Console.WriteLine("Retrieved: {0} of {1}", this.volumes.Count, total);

                // Create list with all placeholders and set to list view source.
                var data = new List<VolumeController>(this.volumes);
                data.AddRange(new VolumeController[total - this.volumes.Count]);
                this.listView.ItemsSource = data;

                // Display errors if any.
                if (error != null)
                {
                    this.DisplayAlert("Error", error, "OK");
                }
            });

            this.currentPage += 1;
        }

        /// <summary>
        /// Item appearing on list view.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        private void ListView_ItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            System.Console.WriteLine("Item appearing: {0}", e.ItemIndex);

            if (e.ItemIndex >= this.currentPage * 20)
            {
                this.GetRemoteData();
            }
        }

        /// <summary>
        /// Item selected on list view.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        private async void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                this.activityIndicator.IsRunning = true;
                await this.Navigation.PushAsync(new BookDetailsPage
                {
                    BindingContext = e.SelectedItem as VolumeController,
                });
                this.activityIndicator.IsRunning = false;
            }
        }
    }
}
