﻿// <copyright file="VolumeController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Books.ModelControllers
{
    using System;
    using System.IO;
    using System.Net;
    using Newtonsoft.Json;

    /// <summary>
    /// Volume controller.
    /// </summary>
    public class VolumeController : Models.Volume
    {
        private static readonly int MaxResults = 20;

        /// <summary>
        /// Gets remote data for query at page.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="page">The page.</param>
        /// <param name="callback">The callback.</param>
        public static async void GetRemoteDataForQueryAtPage(string query, uint page, Action<int, string, VolumeController[]> callback)
        {
            if (!(query?.Length > 0))
            {
                callback?.Invoke(0, "The query is invalid.", null);
            }
            else
            {
                var path = $"https://www.googleapis.com/books/v1/volumes?q={query:ToLower}&startIndex={page * MaxResults}&maxResults={MaxResults}";
                var request = WebRequest.CreateHttp(path);
                request.Method = "GET";

                try
                {
                    var response = await request.GetResponseAsync() as HttpWebResponse;
                    var stringResponse = await new StreamReader(response.GetResponseStream()).ReadToEndAsync();
                    var container = JsonConvert.DeserializeObject<VolumeContainer>(stringResponse);
                    callback?.Invoke(container.TotalItems, null, container.Items);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    callback?.Invoke(0, e.Message, null);
                }
            }
        }

        /// <summary>
        /// Volume container model.
        /// </summary>
        private class VolumeContainer
        {
            /// <summary>
            /// Gets or sets the total items.
            /// </summary>
            /// <value>The total items.</value>
            public int TotalItems { get; set; }

            /// <summary>
            /// Gets or sets the items.
            /// </summary>
            /// <value>The items.</value>
            public VolumeController[] Items { get; set; }
        }
    }
}
