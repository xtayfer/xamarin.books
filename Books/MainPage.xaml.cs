﻿// <copyright file="MainPage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Books
{
    using System;
    using Xamarin.Forms;

    /// <summary>
    /// Main page.
    /// </summary>
    public partial class MainPage : ContentPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainPage"/> class.
        /// </summary>
        public MainPage()
        {
            this.InitializeComponent();
        }

        /// <inheritdoc/>
        protected override void OnAppearing()
        {
            base.OnAppearing();

            // Set actions.
            this.searchBar.SearchButtonPressed += this.SearchBar_SearchButtonPressed;
        }

        /// <summary>
        /// Search button pressed on search bar.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        private async void SearchBar_SearchButtonPressed(object sender, EventArgs e)
        {
            var searchBar = sender as SearchBar;

            // Push book list to query if it's not empty.
            if (searchBar.Text.Length > 0)
            {
                this.activityIndicator.IsRunning = true;
                await this.Navigation.PushAsync(new BookListPage
                {
                    Query = searchBar.Text,
                });
                this.activityIndicator.IsRunning = false;
            }
        }
    }
}
